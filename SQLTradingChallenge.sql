--- 3 days that had the biggest range between opening and closing prices

-- display date 1; date 2 ; date 3 in desc order (biggerst - lowest) 

-- trading range per date 

-- time during the day that stock reached its max price 


CREATE PROCEDURE bigRange

AS

SELECT DISTINCT TOP 3 [Date], abs([open]-[close]) AS Biggest_Range
INTO #mytemptable
FROM trading
ORDER BY Biggest_Range desc 


-- table 2 = date and max high

SELECT DISTINCT top 4 [Date] as Date, max(High) AS Max_High
INTO #tempt2
FROM trading
WHERE [Date] IN 
(SELECT [Date] 
FROM #mytemptable)
GROUP BY [Date]
ORDER BY Max_High desc

-- table 3 = date and time of maxhigh

SELECT DISTINCT top 4 trading.[Date] As Date, [Time] As Max_Time
INTO #timetemp
FROM trading, #tempt2
WHERE trading.[Date] = #tempt2.[Date] AND trading.high = Max_High
GROUP BY trading.[Date], [Time]
ORDER BY [Date] ASC


-- join 1st and last temp table
SELECT t1.[Date] As Date, t1.Biggest_Range, t3.Max_Time As "Time of Max Price"
FROM #mytemptable t1 left join #timetemp t3 On 
t1.[Date] = t3.[Date] 


 DROP PROCEDURE bigRange

   exec bigRange