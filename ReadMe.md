SQL Challenge

1. I assume that 'close' means the trade price 
   I assume that ticker is the symbol


2. To get the amount of characters run the script. My Script is called SQLScript. This is run by doing ./SQLScript 1 sample_dataset2.csv

To run the SQL procedure, open Challenges.sql file and highlight exec VolDate '201010110810' and hit F5

3. Field Size

CREATE TABLE [dbo].[Q2A](
	[ticker] [varchar](4) NOT NULL,
	[date] [varchar](12) NOT NULL,
	[open] [decimal](8, 4) NOT NULL,
	[high] [decimal](8, 4) NOT NULL,
	[low] [decimal](8, 4) NOT NULL,
	[close] [decimal](8, 4) NOT NULL,
	[vol] [varchar](7) NOT NULL
) ON [PRIMARY]

GO

BUT... I had to convert ticker into int so... the new database table is Q2AA

 Select ticker, [date], [open], [high], [low], [close],
  convert(int, vol)
  AS vol
  INTO Q2AA
  FROM Q2A


CREATE TABLE [dbo].[Q2AA](
	[ticker] [varchar](4) NOT NULL,
	[date] [varchar](12) NOT NULL,
	[open] [decimal](8, 4) NOT NULL,
	[high] [decimal](8, 4) NOT NULL,
	[low] [decimal](8, 4) NOT NULL,
	[close] [decimal](8, 4) NOT NULL,
	[vol] [int] NULL
) ON [PRIMARY]

GO


---------------------------------------------------------------


SQL TRADING CHALLENGE 


1. I assume that the biggest range is working out by substracting open from minus 

2.  My Script is called SQLScript. This is run by doing ./SQLScript 1 sample_dataset3.csv

Tp run the SQL procedure, run SQLTradingChallange.sql and highlight exec bigRange and hit F5

3. Field Size 

CREATE TABLE [dbo].[trading](
	[Date] [varchar](10) NOT NULL,
	[Time] [varchar](4) NOT NULL,
	[Open] [decimal](5, 2) NOT NULL,
	[High] [decimal](5, 2) NOT NULL,
	[Low] [decimal](5, 2) NOT NULL,
	[Close] [decimal](5, 2) NOT NULL,
	[Volume] [varchar](7) NOT NULL
) ON [PRIMARY]

GO




  