  --- CONVERT TICKER INTO INT 
  
--  Select ticker, [date], [open], [high], [low], [close],
 -- convert(int, vol)
 -- AS vol
 -- INTO Q2AA
 -- FROM Q2A
  

 CREATE PROCEDURE VolDate
  @EnteredDateTime varchar(12)

  AS 

  -- output needs the volume weighted price
   SELECT ticker, 
   SUM(vol*[close]) /SUM(vol) as VOL_WEIGHTED_PRICE,substring(@EnteredDateTime,1,8)+' '+
   substring(@EnteredDateTime,9,2)+':'+substring(@EnteredDateTime,11,2) AS Date,

   'START: (' + substring(convert(varchar(12),convert(datetime,
   (substring(@EnteredDateTime,1,8)+' '+
   substring(@EnteredDateTime,9,2)+ ':'+ substring(@EnteredDateTime,11,2) )),108),1,5)+ 
   ') - 

	END: ('+
	substring(convert(varchar(12), DATEADD(hh,+5, convert (datetime, 
	(substring(@EnteredDateTime,1,8) +' '+ substring(@EnteredDateTime,9,2)+':'+ substring(@EnteredDateTime,11,2)))),108),1,5)+')' AS Interval

   FROM Q2AA
   WHERE convert(datetime,(substring([date],1,8)+' '+
   substring([date],9,2)+':'+substring([date],11,2)))

   BETWEEN 

  convert(datetime,(substring(@EnteredDateTime,1,8)+' '+
  substring(@EnteredDateTime,9,2)+':'+substring(@EnteredDateTime,11,2))) AND DATEADD(hh,+5,
  convert(datetime,(substring(@EnteredDateTime,1,8)+' '+
  substring(@EnteredDateTime,9,2)+':'+substring(@EnteredDateTime,11,2))) )

  
   GROUP BY ticker

   DROP PROCEDURE VolDate

   exec VolDate '201010110810'

  
 
 -- date in the following format dd/mm/yyyy
 -- specific 5 hout interval start hh.mm - end hh.mm

  -- WHERE [date] BETWEEN @EnteredDateTime AND DATEADD(hh, +5, convert(datetime,
  --(substring([date],1,8)+'-'+
  -- substring([date],9,2)+'-'+substring([date],7,2)
  -- ))
 -- GROUP By ticker


  -- stored procedure for getting date
 -- CREATE PROCEDURE getDate
--  @UserStartDate date(01-01-2010),
 -- @userEndDate date(12-12-2020)

--  AS 
  ---WHERE date BETWEEN @UserStartDate AND @UserEndDate
 

--- getting date in the right format ---- 
----Select substring([date],1,4)+'-'+substring([date],5,2)+'-'+substring([date],7,2) 
---As Date,
--'START: (' +substring(convert(varchar(12),convert(datetime, 
---(substring([date],1,8)+' '+
----substring([date],9,2)+':'+substring([date],11,2) )),108),1,5)+') - END: ('+5hh+')' As Time 
----FROM Q2A--